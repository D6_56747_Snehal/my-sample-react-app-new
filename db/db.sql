

create table Emp (
    empid integer primary key auto_increment,
    name VARCHAR(200),
    salary float,
    age integer
);

insert into Emp (empid, name, salary, age) values  (1,'ram',50000,25);
insert into Emp (empid, name, salary, age) values  (2,'maya',60000,24);
insert into Emp (empid, name, salary, age) values  (3,'riya',50000,23);


--select * from Emp;

--insert into emp (empid, name, salary, age) values  (4,'raman',60000,26);

--update Emp set salary = ? where empid=?;

--delete from Emp where empid=?;
