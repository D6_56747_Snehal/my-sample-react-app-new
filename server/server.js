const express= require('express')

const mysql=require('mysql')

function openDBconnection()
{

const connection= mysql.createConnection({
    host:'localhost',
    port:'3306',
    user:'root',
    password:'manager',
    database:'dec10'
})

Connection.connect()
return Connection()
}

const app = express()
app.use(express.json())

//get
app.get('/get',(request,response)=>{
    const connection=openDBconnection()
    const statement=`select empid, name, salary, age from Emp`
    connection.query(statement,(error,data)=>{
        connection.end()

        if(error){
            response.send(error)
        }
        else{
            response.send(data)
        }
    })
})

//post
app.post('/post',(request,response)=>{
    const{empid, name, salary, age}= request.body
    const connection=openDBconnection()
    const statement=`insert into emp (empid, name, salary, age) values  (${empid},'${name}',${salary},${age})`
    connection.query(statement,(error,data)=>{
        connection.end()

        if(error){
            response.send(error)
        }
        else{
            response.send(data)
        }
    })
})



//update
app.put('/put/:empid',(request,response)=>{
    const{empid}= request.params
    const connection=openDBconnection()
    const statement=`update Emp set salary = ${salary} where empid=${empid}`
    connection.query(statement,(error,data)=>{
        connection.end()

        if(error){
            response.send(error)
        }
        else{
            response.send(data)
        }
    })
})

//delete
app.delete('/delete/:empid',(request,response)=>{
    const{empid}= request.params
    const connection=openDBconnection()
    const statement=`delete from  Emp  where empid=${empid}`
    connection.query(statement,(error,data)=>{
        connection.end()

        if(error){
            response.send(error)
        }
        else{
            response.send(data)
        }
    })
})



app.listen(4000,()=>{
    console.log('port started on 4000')
})

